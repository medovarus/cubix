# cubix

Simple cube removal game on VueJS. You can play it on [https://medovarus.gitlab.io/cubix/](https://medovarus.gitlab.io/cubix/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
